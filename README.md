# PuTTY Ubuntu Theme

Este repositorio proporciona un archivo de registro (`.reg`) que configura un tema personalizado para la sesión de PuTTY en Windows, con una apariencia similar a la de Ubuntu.

## Descripción

Este archivo de registro modifica la configuración de la sesión de PuTTY llamada "Ubuntu" en Windows. Aplica un esquema de colores personalizado para que la interfaz de PuTTY se asemeje visualmente a la apariencia de Ubuntu.

## Uso

1. Descarga el archivo `putty-ubuntu.reg`.
2. Haz doble clic en el archivo `.reg` para importar los cambios en el registro de Windows.
3. Abre PuTTY y selecciona la sesión "Ubuntu" para aplicar el nuevo tema de colores.

## Para aplicar el tema manualmente desde la ventana de configuración de PuTTY:
  - Asegúrate de generar una sesion y guardarla
  - A la izquierda, ve a Windows -> Appearance
  - Cambiar la fuente. PuTTY no tiene Mono, así que use Consolas, tamaño 10pt o Lucinda Console puede ser otra opcion.
  - Luego en Windows -> Colours
  - Haga clic en los colores para editar sus cuadros RGB:

Name | R | G | B
:---- | :----: | :-----: | :-----:
Default Foreground| 238| 238| 236     
Default Bold Foreground | 238| 238| 236                 
Default Background|  48|  10|  36                
Default Bold Background |  48|  10|  36   
Cursor Text| 255| 255| 255 
Cursor Colour| 187| 187| 187
ANSI Black|  46|  52|  54
ANSI Black Bold|  85|  87|  83
ANSI Red| 204|   0|   0
ANSI Red Bold| 239|  41|  41
ANSI Green|  78| 154|   6
ANSI Green Bold| 138| 226|  52
ANSI Yellow| 196| 160|   0
ANSI Yellow Bold| 255| 233|  79
ANSI Blue|  52| 101|164
ANSI Blue Bold| 114 | 159| 207
ANSI Magenta| 117 |  80| 123
ANSI Magenta Bold| 173 | 127|  16
ANSI Cyan|   6 | 152| 154
ANSI Cyan Bold|  52 | 226| 226
ANSI White| 211 | 215| 207
ANSI White Bold| 238 | 238| 236
